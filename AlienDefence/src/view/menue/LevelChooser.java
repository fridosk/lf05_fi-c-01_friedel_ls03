package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import net.miginfocom.swing.MigLayout;
import view.game.GameGUI;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.LayoutStyle.ComponentPlacement;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user, String source) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0,0,screenSize.width, screenSize.height);
		setBackground(Color.BLACK);
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		Dimension screenSize1 = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0,0,screenSize1.width, screenSize1.height);
		pnlButtons.setForeground(Color.WHITE);
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);
						
								JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
								btnDeleteLevel.setBackground(Color.WHITE);
								btnDeleteLevel.addActionListener(new ActionListener() {
									public void actionPerformed(ActionEvent e) {
										btnDeleteLevel_Clicked();
									}
								});
								
										JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
										btnUpdateLevel.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent e) {
												btnUpdateLevel_Clicked();
											}
										});
										
												JButton btnNewLevel = new JButton("Neues Level");
												btnNewLevel.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent arg0) {
														btnNewLevel_Clicked();
													}
												});
												
												JButton btnSpielen = new JButton("Spielen");
												btnSpielen.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent e) {
														btnSpielen_Clicked(alienDefenceController, user);
													}
												});
								GroupLayout gl_pnlButtons = new GroupLayout(pnlButtons);
								gl_pnlButtons.setHorizontalGroup(
									gl_pnlButtons.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_pnlButtons.createSequentialGroup()
											.addGap(243)
											.addComponent(btnSpielen)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(btnNewLevel)
											.addGap(5)
											.addComponent(btnUpdateLevel)
											.addGap(5)
											.addComponent(btnDeleteLevel))
								);
								gl_pnlButtons.setVerticalGroup(
									gl_pnlButtons.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_pnlButtons.createParallelGroup(Alignment.BASELINE)
											.addComponent(btnNewLevel)
											.addComponent(btnSpielen))
										.addComponent(btnUpdateLevel)
										.addComponent(btnDeleteLevel)
								);
								pnlButtons.setLayout(gl_pnlButtons);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
		
		if(source.equals("Testen")){
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
		}else if(source.equals("Leveleditor")) {
			btnSpielen.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	
	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, User user) {
        //Level_id des selektierten Elements auslesen
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
        
        //gewähltes Level aus der Persistenz holen
        Level level = alienDefenceController.getLevelController().readLevel(level_id);
        
        //Gameprozess starten
        Thread t = new Thread("GameThread") {

            @Override
            public void run() {

                // Spielaufruf durchführen
                GameController gameController = alienDefenceController.startGame(level, user);
                new GameGUI(gameController).start();

            }
        };
        //Prozess starten
        t.start();
        //Levelauswahlfenster schließen
        this.leveldesignWindow.dispose();
	}
}
