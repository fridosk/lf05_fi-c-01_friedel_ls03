package form_aendern;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Formv2 {

	private JFrame frame;
	private JLabel lblTest;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Formv2 window = new Formv2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Formv2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblTest = new JLabel("Test");
		lblTest.setForeground(Color.WHITE);
		lblTest.setBackground(Color.WHITE);
		lblTest.setBounds(168, 35, 56, 16);
		frame.getContentPane().add(lblTest);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTest.setText("Changed");
				lblTest.setForeground(Color.red);
			}
		});
		btnRot.setBounds(68, 137, 97, 25);
		frame.getContentPane().add(btnRot);
		
		
		JButton btnGruen = new JButton("New button");
		btnGruen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblTest.setForeground(Color.green);
			}
		});
		btnGruen.setBounds(262, 137, 97, 25);
		frame.getContentPane().add(btnGruen);
		
		
		
		
		//tets//

	}
}
